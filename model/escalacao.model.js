const mongoose = require('mongoose')

const escalacaoSchema = mongoose.Schema({
    nomeJogador: String,
    posicao: String,
    foto: String,
})

module.exports = mongoose.model('Escalacao', escalacaoSchema)