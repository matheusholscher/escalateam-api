const express = require('express')
const routes = require('./routes/routes')

const app = express()

app.use(express.json())

routes(app)

app.listen(8080, () => {
    console.log('server is up!')
})


